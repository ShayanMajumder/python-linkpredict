from setuptools import setup, find_packages

setup(
    name="linkpredict",
    version="2.1.0",
    description="A generic and dynamic link budget tool",
    license="MIT",
    python_requires='>=3',
    packages=find_packages(),
    install_requires=['numpy', 'scipy', 'skyfield'],
)
